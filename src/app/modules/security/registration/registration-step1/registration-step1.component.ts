import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomerDataLoaderService} from '../../../../services/customer-data-loader.service';
import {Customer} from '../../../../models/customer';
import {Router} from '@angular/router';
import {Title} from '@angular/platform-browser';

@Component({
    selector: 'app-registration-step1',
    templateUrl: './registration-step1.component.html',
    styleUrls: ['./registration-step1.component.scss']
})
export class RegistrationStep1Component implements OnInit {

    form: FormGroup;
    private customer: Customer;

    constructor(private fb: FormBuilder,
                private dataLoader: CustomerDataLoaderService,
                private router: Router,
                private titleService: Title) {
    }

    ngOnInit() {

        // @ts-ignore
        this.customer = this.dataLoader.loadCustomer('customer', 'session');
        if (this.customer === null) {
            this.customer = new Customer();
        }
        this.titleService.setTitle('Registration Personal information - Wunder Mobility');
        this.buildForm();
    }

    submit() {
        if (this.form.valid) {
            const formData = this.form.value;
            this.customer.firstName = formData.firstName;
            this.customer.lastName = formData.lastName;
            this.customer.telephone = formData.telephone;
            this.dataLoader.saveCustomer(this.customer, 'session');

            sessionStorage.setItem('next', 'true');
            this.router.navigate(['/registration/step-2']);
        }
    }

    private buildForm() {

        this.form = this.fb.group({
            firstName: [this.customer.firstName, Validators.compose([
                Validators.required,
            ])],
            lastName: [this.customer.lastName, Validators.compose([
                Validators.required,
            ])],
            telephone: [this.customer.telephone, Validators.compose([
                Validators.required,
            ])],
        });
    }
}
