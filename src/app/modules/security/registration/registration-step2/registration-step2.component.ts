import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomerDataLoaderService} from '../../../../services/customer-data-loader.service';
import {Router} from '@angular/router';
import {Customer} from '../../../../models/customer';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-registration-step2',
  templateUrl: './registration-step2.component.html',
  styleUrls: ['./registration-step2.component.scss']
})
export class RegistrationStep2Component implements OnInit {

    form: FormGroup;
    private customer: Customer;

    constructor(private fb: FormBuilder,
                private dataLoader: CustomerDataLoaderService,
                private router: Router,
                private titleService: Title) {
    }

    ngOnInit() {

        // @ts-ignore
        this.customer = this.dataLoader.loadCustomer('customer', 'session');
        this.titleService.setTitle('Registration Address information - Wunder Mobility');
        this.buildForm();
    }

    submit() {
        if (this.form.valid) {

            const formData = this.form.value;

            this.customer.address = formData.address;
            this.customer.houseNumber = formData.houseNumber;
            this.customer.city = formData.city;
            this.customer.zipCode = formData.zipCode;
            this.dataLoader.saveCustomer(this.customer, 'session');
            // @ts-ignore
            this.router.navigate(['/registration/step-3']);
        }
    }

    previous() {
        sessionStorage.setItem('previous', 'true');
        this.router.navigate(['/registration/step-1']);
    }

    private buildForm() {

        this.form = this.fb.group({
            address: [this.customer.address, Validators.compose([
                Validators.required,
            ])],
            houseNumber: [this.customer.houseNumber, Validators.compose([
                Validators.required,
                Validators.min(1),
            ])],
            city: [this.customer.city, Validators.compose([
                Validators.required,
            ])],
            zipCode: [this.customer.zipCode, Validators.compose([
                Validators.required,
                Validators.min(1),
            ])],
        });
    }

}
