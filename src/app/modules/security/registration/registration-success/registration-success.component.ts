import {Component, OnInit} from '@angular/core';
import {CustomerDataLoaderService} from '../../../../services/customer-data-loader.service';
import {Customer} from '../../../../models/customer';
import {Title} from '@angular/platform-browser';

@Component({
    selector: 'app-registration-success',
    templateUrl: './registration-success.component.html',
    styleUrls: ['./registration-success.component.scss']
})

export class RegistrationSuccessComponent implements OnInit {

    customer: Customer;

    constructor(private dataLoader: CustomerDataLoaderService, private titleService: Title) {
    }

    ngOnInit() {

        this.customer = this.dataLoader.loadCustomer('customer') as Customer;
        this.titleService.setTitle('Registration Success - Wunder Mobility');
    }
}
