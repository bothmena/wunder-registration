import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Customer} from '../../../../models/customer';
import {CustomerDataLoaderService} from '../../../../services/customer-data-loader.service';
import {Router} from '@angular/router';
import {BackendService} from '../../../../services/backend.service';
import {HttpErrorResponse} from '@angular/common/http';
import {AppGlobals} from '../../../../globals';
import {Title} from '@angular/platform-browser';


@Component({
    selector: 'app-registration-step3',
    templateUrl: './registration-step3.component.html',
    styleUrls: ['./registration-step3.component.scss']
})
export class RegistrationStep3Component implements OnInit {

    form: FormGroup;
    private customer: Customer;
    isLoading = false;
    errorMessage = '';

    constructor(private fb: FormBuilder,
                private dataLoader: CustomerDataLoaderService,
                private backService: BackendService,
                private router: Router,
                private titleService: Title) {
    }

    ngOnInit() {

        // @ts-ignore
        this.customer = this.dataLoader.loadCustomer('customer', 'session');
        this.titleService.setTitle('Registration Payment information - Wunder Mobility');
        this.buildForm();
    }

    submit() {
        if (this.form.valid) {

            this.isLoading = true;
            this.backService.postPaymentData(this.form.value).subscribe(
                (response) => {

                    this.errorMessage = '';
                    this.isLoading = false;
                    // removing the user data from session storage and saving it in local storage.
                    sessionStorage.removeItem(AppGlobals.CUSTOMER_DB_KEY);
                    this.customer.paymentDataId = response.paymentDataId;
                    this.dataLoader.saveCustomer(this.customer, 'local');
                    this.router.navigate(['/registration/success']);
                },
                (error: HttpErrorResponse) => {

                    this.isLoading = false;
                    switch (error.status) {
                        case 404:
                            this.errorMessage = 'Url of the backend endpoint can not be found!';
                            break;
                        case 500:
                            this.errorMessage = 'Backed is returning server error, please try again later!';
                            break;
                        default:
                            this.errorMessage = 'Something went wrong, please make sure your data is clear!';
                            break;
                    }
                },
            );
        }
    }

    previous() {
        sessionStorage.setItem('previous', 'true');
        this.router.navigate(['/registration/step-2']);
    }

    private buildForm() {

        const min = 1;
        const max = 1000;
        const randomId = Math.floor(Math.random() * (+max - +min)) + +min;

        this.form = this.fb.group({
            customerId: [randomId],
            iban: ['', Validators.compose([
                Validators.required,
            ])],
            owner: ['', Validators.compose([
                Validators.required,
            ])],
        });
    }
}
