import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PageNotFoundComponent} from './page-not-found.component';
import {Title} from '@angular/platform-browser';

describe('PageNotFoundComponent', () => {
    let component: PageNotFoundComponent;
    let fixture: ComponentFixture<PageNotFoundComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PageNotFoundComponent],
            providers: [Title]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PageNotFoundComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should have a paragraph to explain the error', () => {
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('[data-test="explanation"]')).toBeTruthy();
    });

    it('should have a link to homepage', () => {
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('a[data-test="homepage-link"]')).toBeTruthy();
    });

    it('should have a valid url to homepage', () => {
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('a[routerLink=""]')).toBeTruthy();
    });

    it(`should have a title containing 'Page not found'`, () => {

        const titleService = TestBed.get(Title);
        expect(titleService.getTitle()).toContain('Page not found');
    });

    it(`should have a title containing 'Wunder Mobility'`, () => {

        const titleService = TestBed.get(Title);
        expect(titleService.getTitle()).toContain('Wunder Mobility');
    });
});
