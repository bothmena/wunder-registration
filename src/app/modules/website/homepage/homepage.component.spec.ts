import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HomepageComponent} from './homepage.component';
import {Title} from '@angular/platform-browser';

describe('HomepageComponent', () => {
    let component: HomepageComponent;
    let fixture: ComponentFixture<HomepageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HomepageComponent],
            providers: [Title]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HomepageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it(`should have a title containing 'Home'`, () => {

        const titleService = TestBed.get(Title);
        expect(titleService.getTitle()).toContain('Home');
    });

    it(`should have a title containing 'Wunder Mobility'`, () => {

        const titleService = TestBed.get(Title);
        expect(titleService.getTitle()).toContain('Wunder Mobility');
    });
});
