export class Customer {
    private _id: number;
    private _firstName: string;
    private _lastName: string;
    private _telephone: string;
    private _address: string;
    private _houseNumber: number;
    private _zipCode: number;
    private _city: string;
    private _paymentDataId: string;

    constructor(id: number = null, firstName: string = null, lastName: string = null, telephone: string = null, address: string = null,
                houseNumber: number = null, zipCode: number = null, city: string = null, paymentDataId: string = null) {
        this._id = id;
        this._firstName = firstName;
        this._lastName = lastName;
        this._telephone = telephone;
        this._address = address;
        this._houseNumber = houseNumber;
        this._zipCode = zipCode;
        this._city = city;
        this._paymentDataId = paymentDataId;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get firstName(): string {
        return this._firstName;
    }

    set firstName(value: string) {
        this._firstName = value;
    }

    get lastName(): string {
        return this._lastName;
    }

    set lastName(value: string) {
        this._lastName = value;
    }

    get telephone(): string {
        return this._telephone;
    }

    set telephone(value: string) {
        this._telephone = value;
    }

    get address(): string {
        return this._address;
    }

    set address(value: string) {
        this._address = value;
    }

    get houseNumber(): number {
        return this._houseNumber;
    }

    set houseNumber(value: number) {
        this._houseNumber = value;
    }

    get zipCode(): number {
        return this._zipCode;
    }

    set zipCode(value: number) {
        this._zipCode = value;
    }

    get city(): string {
        return this._city;
    }

    set city(value: string) {
        this._city = value;
    }

    get paymentDataId(): string {
        return this._paymentDataId;
    }

    set paymentDataId(value: string) {
        this._paymentDataId = value;
    }
}
