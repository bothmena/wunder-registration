export class AppGlobals {
    public static CUSTOMER_DB_KEY = 'customer';
}

export class ErrorMessage {
    public static LOADER_SERVICE_AS_ARG = 'as should be either customer or object';
    public static LOADER_SERVICE_WHERE_ARG = 'where should be either local for localStorage or session for sessionStorage';
}
