import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {CustomerDataValidatorService} from './customer-data-validator.service';

@Injectable({
    providedIn: 'root'
})
export class SuccessGuardService implements CanActivate {

    constructor(private dataValidator: CustomerDataValidatorService, private router: Router) {
    }

    canActivate(): boolean {

        if (this.dataValidator.isRegistrationSuccessful()) {
            return true;
        } else {
            this.router.navigate(['/registration/step-3']);
            return false;
        }
    }
}
