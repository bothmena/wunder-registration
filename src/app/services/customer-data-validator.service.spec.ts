import {TestBed} from '@angular/core/testing';

import {CustomerDataValidatorService} from './customer-data-validator.service';
import {Customer} from '../models/customer';
import {CustomerDataLoaderService} from './customer-data-loader.service';

describe('CustomerDataValidatorService.creation', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: CustomerDataValidatorService = TestBed.get(CustomerDataValidatorService);
        expect(service).toBeTruthy();
    });
});

describe('CustomerDataValidatorService', () => {

    // tests set up

    let validCustomer: Customer;
    let dataLoader: CustomerDataLoaderService;

    beforeEach(() => TestBed.configureTestingModule({
        providers: [
            CustomerDataLoaderService,
            CustomerDataValidatorService
        ],
    }));

    beforeEach(() => {
        validCustomer = new Customer(1, 'abcd', 'abcd', 'abcd', 'abcd', 5,
            5, 'abcd', 'abcd');

        dataLoader = TestBed.get(CustomerDataLoaderService);
        spyOn(dataLoader, 'loadCustomer').and.returnValue(validCustomer);
    });

    // Testing isStep1DataValid method

    it('should recognize invalid data for step 1, firstName null', () => {
        validCustomer.firstName = null;
        const service: CustomerDataValidatorService = TestBed.get(CustomerDataValidatorService);

        expect(service.isStep1DataValid()).toBe(false);
    });

    it('should recognize invalid data for step 1, firstName empty', () => {
        validCustomer.firstName = '';
        const service: CustomerDataValidatorService = TestBed.get(CustomerDataValidatorService);

        expect(service.isStep1DataValid()).toBe(false);
    });

    it('should recognize invalid data for step 1, lastName null', () => {
        validCustomer.lastName = null;
        const service: CustomerDataValidatorService = TestBed.get(CustomerDataValidatorService);

        expect(service.isStep1DataValid()).toBe(false);
    });

    it('should recognize invalid data for step 1, lastName empty', () => {
        validCustomer.lastName = '';
        const service: CustomerDataValidatorService = TestBed.get(CustomerDataValidatorService);

        expect(service.isStep1DataValid()).toBe(false);
    });

    it('should recognize invalid data for step 1, telephone null', () => {
        validCustomer.telephone = null;
        const service: CustomerDataValidatorService = TestBed.get(CustomerDataValidatorService);

        expect(service.isStep1DataValid()).toBe(false);
    });

    it('should recognize invalid data for step 1, telephone empty', () => {
        validCustomer.telephone = '';
        const service: CustomerDataValidatorService = TestBed.get(CustomerDataValidatorService);

        expect(service.isStep1DataValid()).toBe(false);
    });

    it('should recognize if step 1 data are valid', () => {
        const service: CustomerDataValidatorService = TestBed.get(CustomerDataValidatorService);

        expect(service.isStep1DataValid()).toBe(true);
    });

    // Testing isStep2DataValid method

    it('should recognize invalid data for step 2, address null', () => {
        validCustomer.address = null;
        const service: CustomerDataValidatorService = TestBed.get(CustomerDataValidatorService);

        expect(service.isStep2DataValid()).toBe(false);
    });

    it('should recognize invalid data for step 2, address empty', () => {
        validCustomer.address = '';
        const service: CustomerDataValidatorService = TestBed.get(CustomerDataValidatorService);

        expect(service.isStep2DataValid()).toBe(false);
    });

    it('should recognize invalid data for step 2, houseNumber null', () => {
        validCustomer.houseNumber = null;
        const service: CustomerDataValidatorService = TestBed.get(CustomerDataValidatorService);

        expect(service.isStep2DataValid()).toBe(false);
    });

    it('should recognize invalid data for step 2, zipCode null', () => {
        validCustomer.zipCode = null;
        const service: CustomerDataValidatorService = TestBed.get(CustomerDataValidatorService);

        expect(service.isStep2DataValid()).toBe(false);
    });

    it('should recognize invalid data for step 2, city null', () => {
        validCustomer.city = null;
        const service: CustomerDataValidatorService = TestBed.get(CustomerDataValidatorService);

        expect(service.isStep2DataValid()).toBe(false);
    });

    it('should recognize invalid data for step 2, city empty', () => {
        validCustomer.city = '';
        const service: CustomerDataValidatorService = TestBed.get(CustomerDataValidatorService);

        expect(service.isStep2DataValid()).toBe(false);
    });

    it('should recognize if step 2 data are valid', () => {
        const service: CustomerDataValidatorService = TestBed.get(CustomerDataValidatorService);

        expect(service.isStep2DataValid()).toBe(true);
    });

    // Testing isRegistrationSuccessful method

    it('should recognize if registration is unsuccessful, paymentDataId null', () => {
        validCustomer.paymentDataId = null;
        const service: CustomerDataValidatorService = TestBed.get(CustomerDataValidatorService);

        expect(service.isRegistrationSuccessful()).toBe(false);
    });

    it('should recognize if registration is unsuccessful, paymentDataId empty', () => {
        validCustomer.paymentDataId = '';
        const service: CustomerDataValidatorService = TestBed.get(CustomerDataValidatorService);

        expect(service.isRegistrationSuccessful()).toBe(false);
    });

    it('should recognize if registration is successful', () => {
        const service: CustomerDataValidatorService = TestBed.get(CustomerDataValidatorService);

        expect(service.isRegistrationSuccessful()).toBe(true);
    });
});
