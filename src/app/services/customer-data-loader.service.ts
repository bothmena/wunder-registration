import {Injectable} from '@angular/core';
import {Customer} from '../models/customer';
import {AppGlobals, ErrorMessage} from '../globals';

@Injectable({
    providedIn: 'root'
})
export class CustomerDataLoaderService {

    private static toCustomer(dictionary: object): Customer {

        if (dictionary === null) {
            return null;
        }

        const getValue = (key: string) => {
            return (dictionary.hasOwnProperty(key) ? dictionary[key] : null);
        };

        return new Customer(getValue('id'), getValue('firstName'), getValue('lastName'), getValue('telephone'),
            getValue('address'), getValue('houseNumber'), getValue('zipCode'), getValue('city'),
            getValue('paymentDataId'));
    }

    private static toObject(customer: Customer): object {

        if (customer === null) {
            return null;
        }

        return {
            id: customer.id,
            firstName: customer.firstName,
            lastName: customer.lastName,
            telephone: customer.telephone,
            address: customer.address,
            houseNumber: customer.houseNumber,
            zipCode: customer.zipCode,
            city: customer.city,
            paymentDataId: customer.paymentDataId,
        };
    }

    constructor() {
    }

    loadCustomer(as: string = 'object', where: string = 'local') {

        let data: object;
        if (where === 'local') {
            data = JSON.parse(localStorage.getItem(AppGlobals.CUSTOMER_DB_KEY));
        } else if (where === 'session') {
            data = JSON.parse(sessionStorage.getItem(AppGlobals.CUSTOMER_DB_KEY));
        } else {
            throw Error(ErrorMessage.LOADER_SERVICE_WHERE_ARG);
        }

        if (as === 'object') {
            return data;
        } else if (as === 'customer') {
            return CustomerDataLoaderService.toCustomer(data);
        } else {
            throw Error(ErrorMessage.LOADER_SERVICE_AS_ARG);
        }
    }

    saveCustomer(customer: Customer, where: string = 'local') {

        const data = JSON.stringify(CustomerDataLoaderService.toObject(customer));
        if (where === 'local') {
            localStorage.setItem(AppGlobals.CUSTOMER_DB_KEY, data);
        } else if (where === 'session') {
            sessionStorage.setItem(AppGlobals.CUSTOMER_DB_KEY, data);
        } else {
            throw Error(ErrorMessage.LOADER_SERVICE_WHERE_ARG);
        }
    }
}
