import { Injectable } from '@angular/core';
import {CanActivate, CanActivateChild, Router} from '@angular/router';
import {CustomerDataValidatorService} from './customer-data-validator.service';

@Injectable({
  providedIn: 'root'
})
export class Step1GuardService implements CanActivate {

    constructor(private dataValidator: CustomerDataValidatorService, private router: Router) {
    }

    canActivate(): boolean {

        if (this.dataValidator.isStep1DataValid()) {

            // if key previous exists in the session storage, this means that this redirection is launched from
            // a previous button.
            if (sessionStorage.getItem('previous')) {
                sessionStorage.removeItem('previous');
                return true;
            }

            this.router.navigate(['/registration/step-2']);
            return false;
        }
        return true;
    }
}
