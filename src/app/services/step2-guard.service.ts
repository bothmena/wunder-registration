import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {CustomerDataValidatorService} from './customer-data-validator.service';

@Injectable({
    providedIn: 'root'
})
export class Step2GuardService implements CanActivate {

    constructor(private dataValidator: CustomerDataValidatorService, private router: Router) {
    }

    canActivate(): boolean {

        if (!this.dataValidator.isStep1DataValid()) {

            this.router.navigate(['/registration/step-1']);
            return false;
        } else if (this.dataValidator.isStep2DataValid()) {

            // if key previous exists in the session storage, this means that this redirection is launched from
            // a previous button.
            if (sessionStorage.getItem('previous')) {
                sessionStorage.removeItem('previous');
                return true;
            }

            // if key next exists in the session storage, this means that this redirection is launched from
            // a next button.
            if (sessionStorage.getItem('next')) {
                sessionStorage.removeItem('next');
                return true;
            }

            this.router.navigate(['/registration/step-3']);
            return false;
        }
        return true;
    }
}
