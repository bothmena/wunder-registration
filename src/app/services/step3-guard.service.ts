import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {CustomerDataValidatorService} from './customer-data-validator.service';

@Injectable({
    providedIn: 'root'
})
export class Step3GuardService implements CanActivate {

    constructor(private dataValidator: CustomerDataValidatorService, private router: Router) {
    }

    canActivate(): boolean {

        if (this.dataValidator.isRegistrationSuccessful()) {
            this.router.navigate(['/registration/success']);
        } else if (!this.dataValidator.isStep2DataValid()) {
            this.router.navigate(['/registration/step-2']);
            return false;
        }
        return true;
    }
}
