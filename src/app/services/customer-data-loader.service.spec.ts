import {TestBed} from '@angular/core/testing';

import {CustomerDataLoaderService} from './customer-data-loader.service';
import {Customer} from '../models/customer';
import {ErrorMessage} from '../globals';

describe('CustomerDataLoaderService', () => {

    const customerDict = {
        id: 5,
        firstName: 'abcd',
        lastName: 'abcd',
        telephone: 'abcd',
        address: 'abcd',
        houseNumber: 5,
        zipCode: 5,
        city: 'abcd',
        paymentDataId: 'abcd',
    };
    const customerObject = new Customer(5, 'abcd', 'abcd', 'abcd', 'abcd',
        5, 5, 'abcd', 'abcd');

    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: CustomerDataLoaderService = TestBed.get(CustomerDataLoaderService);
        expect(service).toBeTruthy();
    });

    it('should load data as object', () => {
        spyOn(localStorage, 'getItem').and.returnValue(JSON.stringify(customerDict));
        const service: CustomerDataLoaderService = TestBed.get(CustomerDataLoaderService);
        const customer = service.loadCustomer('object');

        expect(customer).toEqual(customerDict);
    });

    it('should load data as Customer instance', () => {
        spyOn(localStorage, 'getItem').and.returnValue(JSON.stringify(customerDict));
        const service: CustomerDataLoaderService = TestBed.get(CustomerDataLoaderService);
        const dict = service.loadCustomer('customer');

        expect(dict).toEqual(customerObject);
    });

    it('should return null when no data is saved in the database, as = customer', () => {
        spyOn(localStorage, 'getItem').and.returnValue(null);
        const service: CustomerDataLoaderService = TestBed.get(CustomerDataLoaderService);
        const customer = service.loadCustomer('customer');

        expect(customer).toBe(null);
    });

    it('should return null when no data is saved in the database, as = object', () => {
        spyOn(localStorage, 'getItem').and.returnValue(null);
        const service: CustomerDataLoaderService = TestBed.get(CustomerDataLoaderService);
        const dict = service.loadCustomer('object');

        expect(dict).toBe(null);
    });

    it('should raise an exception when as argument is neither object or customer', () => {
        spyOn(localStorage, 'getItem').and.returnValue(null);
        const service: CustomerDataLoaderService = TestBed.get(CustomerDataLoaderService);
        const dict = service.loadCustomer('object');

        expect(() => {
            service.loadCustomer('something_else');
        }).toThrow(new Error(ErrorMessage.LOADER_SERVICE_AS_ARG));
    });
});
