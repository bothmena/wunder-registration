import {Injectable} from '@angular/core';
import {CustomerDataLoaderService} from './customer-data-loader.service';
import {Customer} from '../models/customer';

@Injectable({
    providedIn: 'root'
})
export class CustomerDataValidatorService {

    constructor(private loaderService: CustomerDataLoaderService) {
    }

    isStep1DataValid() {
        const customer = this.getCustomer();

        if (customer === null) {
            return false;
        } else {
            return Boolean(customer.firstName) && Boolean(customer.lastName) && Boolean(customer.telephone);
        }
    }

    isStep2DataValid() {
        const customer = this.getCustomer();

        if (customer === null) {
            return false;
        } else {
            return Boolean(customer.address) && Boolean(customer.houseNumber) && Boolean(customer.zipCode) && Boolean(customer.city);
        }
    }

    isRegistrationSuccessful() {
        const customer = this.getCustomer();

        if (customer) {
            return Boolean(customer.paymentDataId);
        }
        return false;
    }

    private getCustomer(): Customer {
        let customer = this.loaderService.loadCustomer('customer', 'local');
        if (customer === null) {
            customer = this.loaderService.loadCustomer('customer', 'session');
        }

        return customer as Customer;
    }
}
