import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class BackendService {

    private backendUrl =
        'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';

    constructor(private http: HttpClient) {
    }

    postPaymentData(data: any): Observable<any> {

        const httpOptions = {headers: new HttpHeaders({
                'Content-Type':  'application/json',
                Accept:  'application/json'
        })};

        // return this.http.post(this.backendUrl, JSON.stringify(data), httpOptions);
        return of({paymentDataId: '00d0569fc439be340cbb0389c24e2f55fb5a1e9d79ca64f1dcd6807e2d65ea23d0db296a192bc70c2b93fb78f618aff8'});
    }
}
