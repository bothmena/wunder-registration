import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomepageComponent} from './modules/website/homepage/homepage.component';
import {RegistrationSuccessComponent} from './modules/security/registration/registration-success/registration-success.component';
import {RegistrationComponent} from './modules/security/registration/registration.component';
import {RegistrationStep1Component} from './modules/security/registration/registration-step1/registration-step1.component';
import {RegistrationStep2Component} from './modules/security/registration/registration-step2/registration-step2.component';
import {RegistrationStep3Component} from './modules/security/registration/registration-step3/registration-step3.component';
import {PageNotFoundComponent} from './modules/website/page-not-found/page-not-found.component';
import {Step1GuardService} from './services/step1-guard.service';
import {Step2GuardService} from './services/step2-guard.service';
import {Step3GuardService} from './services/step3-guard.service';
import {SuccessGuardService} from './services/success-guard.service';


const routes: Routes = [
    { path: '', component: HomepageComponent, },
    {
        path: 'registration', component: RegistrationComponent, children: [
            {path: '', redirectTo: 'step-1', pathMatch: 'full'},
            {path: 'step-1', component: RegistrationStep1Component, canActivate: [Step1GuardService]},
            {path: 'step-2', component: RegistrationStep2Component, canActivate: [Step2GuardService]},
            {path: 'step-3', component: RegistrationStep3Component, canActivate: [Step3GuardService]},
            {path: 'success', component: RegistrationSuccessComponent, canActivate: [SuccessGuardService]},
        ],
    },
    { path: '**', component: PageNotFoundComponent, },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
