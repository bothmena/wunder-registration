import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {RegistrationComponent} from './modules/security/registration/registration.component';
import {RegistrationStep1Component} from './modules/security/registration/registration-step1/registration-step1.component';
import {RegistrationStep2Component} from './modules/security/registration/registration-step2/registration-step2.component';
import {RegistrationStep3Component} from './modules/security/registration/registration-step3/registration-step3.component';
import {HomepageComponent} from './modules/website/homepage/homepage.component';
import {RegistrationSuccessComponent} from './modules/security/registration/registration-success/registration-success.component';
import {PageNotFoundComponent} from './modules/website/page-not-found/page-not-found.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import { NavbarComponent } from './shared/components/navbar/navbar.component';
import { FooterComponent } from './shared/components/footer/footer.component';

@NgModule({
    declarations: [
        AppComponent,
        RegistrationComponent,
        RegistrationStep1Component,
        RegistrationStep2Component,
        RegistrationStep3Component,
        HomepageComponent,
        RegistrationSuccessComponent,
        PageNotFoundComponent,
        NavbarComponent,
        FooterComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        ReactiveFormsModule,
        HttpClientModule,
        NgbModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
