# WunderRegistration

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

---

Udacity Git Commit Message Style Guide: https://udacity.github.io/git-styleguide/

### Describe possible optimizations for your code.

1. Regarding the SCSS structure, I am using a simpler version of the 7-1 pattern due to the simplicity of the application, Although that will not stop me from using a scalable architecture for any project just to avoid the headaches of refactoring a project if its complexity grows beyond expectations.
What I can do better is to organize my styles in a more efficient and more organized way to minimize redundancy.
2. The way I make sure that the user comes back at the right page to finish the process. When I started working on this I though a single Guard Service will be enough, but that didn't work out and I had to make a separate service for each step (1, 2, 3 & success). I believe I can find a more simple way to solve this, although the logic for each service is simple and straightforward.

### Which things could be done better, than you’ve done it?

1. The website could be more mobile friendly if I can work on it more.
2. I can make the design of the website better and look much closer to Wunder Mobility website.
3. Implement a logout page (this is a very simple thing to do), the page will simply remove the customer data and redirect the user to the homepage. At the moment you have to delete the customer data manually in order to be able to access the registration pages, which in my opinion makes sense.
4. Instead of saving the whole customer data (personal & address information) to be able to identify him and retrieve his data, I would use Json Web Tokens (for example) to do this, but this of course involves access to the backend part of the website.
5. I started developing the app using TDD, but this took more time than anticipated, so at some point I realized if I continue working with the same strategy I won't be able to finish the project in time, I wish I had more time to finish the project the way I started and to be able to write some e2e tests too.

#### N.B. At the moment my code does not post the payment data to the API endpoint

I have implemented the logic that will post the data to the API endpoint and everything works great, except that the browser block the response because it's missing the Access-Control-Allow-Origin header, this wasn't the first time I encounter this problem, previously I had access to the backend code (I wrote it) and all I had to do is to add the missing header to the response and that fixed the issue. Right now I am faking the call to the endpoint.

#### Fun fact: I worked on a similar task
In a website I was developing months ago, the user have to provide a lot of information to create an account.

The repository of the project can be found here: https://gitlab.com/bothmena/zednilma-client-mdb
the component I am talking about can be found here: https://gitlab.com/bothmena/zednilma-client-mdb/tree/master/src/app/security/init-account

#### Finally,

I hope you like my work and I hope I can hear from you soon :) 
